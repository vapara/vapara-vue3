# DOCUMENTATION

## HOW TO DEBUG

* Press `F5` OR...
* Codium / Debug in Firefox

## First Initialization

see [vue3+typescript+vite](https://stackoverflow.com/questions/63724523/how-to-add-typescript-to-vue-3-and-vite-project#64438876)

## Logo

inspired by [Astrologia: Yantra de Śrī Kamalātmikā Devī](https://sriganesa.blogspot.com/2014/03/yantra-de-sri-kamalatmika-devi.html)

## Hints

### Use Firacode font with ligature

`sudo apt install fonts-firacode`

### Vue3/Typescript

* use `defineComponent` for Component
* `console.log` not accepted except warn and error
* `global.helpers` might help for filter conversions (capitalize, toJune, ...)

