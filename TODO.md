# TODO

## ARCHITECTURE

- [x] vite + vue3 (up-to-date)
- [x] codium debug (Firefox, Chromium)
- [x] git version control awareness
- [x] eslint
- [x] licence support
- [x] composition API
- [x] windi-css
- [x] npm version 7
- [x] store solution based on pure composition API
- [x] strict typescript support
- [x] fake data
  - [ ] images
- [ ] i18n
- [ ] axios or fetch

## FEATURES

- [ ] grab products, categories and images from Dolibarr's API
  - [ ] parallelize
  - [ ] scheduled action
  - [ ] cache for images
- [ ] basket
- [ ] login/logout
