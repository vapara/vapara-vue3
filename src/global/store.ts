import { computed, reactive, readonly } from 'vue'

//CONSTANTS
const TVA = 8.5

type Category = {
  id: number
  label: string
  focus?: boolean
}

type Product = {
  id: number
  label: string
  category_id: number
  price: number
  image?: string
}

// FAKE DATA
const fakeProducts: Product[] = [
  {
    id: 1,
    label: 'Acer S1000',
    category_id: 1,
    price: 1000,
    image: 'https://vapara.mithril.re/images/60/980-000513-1561536472-8357.jpg'
  },
  {
    id: 2,
    label: 'Acer S1300',
    category_id: 1,
    price: 1300,
    image:
      'https://vapara.mithril.re/images/63/CPU_INT_G5400-CPU_INT_G5400-61qXmtRVFoL._AC_SL1000_.jpg'
  },
  {
    id: 3,
    label: 'Lenovo X250',
    category_id: 1,
    price: 1600,
    image:
      'https://vapara.mithril.re/images/64/TPLINK_ARCHER_T1U-external-content.duckduckgo.com.jpg'
  },
  {
    id: 4,
    label: 'Iyama 28" ',
    category_id: 2,
    price: 3000,
    image:
      'https://vapara.mithril.re/images/66/CONT_HEB_DOLIBARR-220px-Dolibarr_logo.png'
  },
  {
    id: 5,
    label: 'Iyama 32" ',
    category_id: 2,
    price: 4500,
    image:
      'https://vapara.mithril.re/images/47/UC_ACER_X2630G-083-101-011-07.jpg'
  },
  {
    id: 6,
    label: 'Seagate 250Gb SSD',
    category_id: 3,
    price: 1000,
    image: 'https://vapara.mithril.re/images/60/980-000513-1561536472-8357.jpg'
  },
  {
    id: 7,
    label: 'Seagate 500Gb SSD',
    category_id: 3,
    price: 1600,
    image: 'https://vapara.mithril.re/images/60/980-000513-1561536472-8357.jpg'
  }
]

const fakeCategories: Category[] = [
  { id: 1, label: 'carte mère' },
  { id: 2, label: 'câble' },
  { id: 3, label: 'divers' },
  { id: 4, label: 'haut-parleur' },
  { id: 5, label: 'imprimante' },
  { id: 6, label: 'moniteur' },
  { id: 7, label: 'ordinateur fixe' },
  { id: 8, label: 'ordinateur portable' },
  { id: 9, label: 'processeur' },
  { id: 10, label: 'réseau' },
  { id: 11, label: 'services' }
]

//STATE
const state = reactive({
  count: 10,
  categories: fakeCategories,
  products: fakeProducts
})

//COMPUTED
const ttc = computed(() => (state.count * (100 + TVA)) / 100)

const filteredProducts = computed(() => {
  const focusedCategory = state.categories.find((c) => c.focus == true)
  if (focusedCategory !== undefined)
    return state.products.filter((p) => p.category_id == focusedCategory.id)
  else return state.products
})

//ARROW FUNCTION
const inc = (amount: number): number => {
  return (state.count += amount)
}

//STANDARD FUNCTION
function reset(): void {
  state.count = 0
}
function toggleFocus(categoryId: number): void {
  const previousFocus = state.categories.find((c) => c.id == categoryId)?.focus
  state.categories = state.categories.map((c) => {
    if (c.id == categoryId) c.focus = !previousFocus
    else c.focus = false
    return c
  })
}

export default {
  state: readonly(state),
  filteredProducts,
  ttc,
  inc,
  reset,
  toggleFocus
}
