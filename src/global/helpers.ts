function capitalize(s: string): string {
  return s.charAt(0).toUpperCase() + s.slice(1)
}

const locale = 'fr-FR' // might be `navigator.language`
const formatter = new Intl.NumberFormat(locale, {
  style: 'currency',
  currency: 'eur'
  // These options are needed to round to whole numbers if that's what you want.
  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
})

function toJune(n: number): string {
  return formatter.format(n).replace('€', 'Ǧ1')
}

export default { capitalize, toJune }
