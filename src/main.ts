import { createApp } from 'vue'
import 'windi.css'

import App from '@/components/App.vue'

declare const APP_TITLE: string
document.title = APP_TITLE

createApp(App).mount('#app')
