import { execSync } from 'child_process'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import { resolve } from "path"

import pkg from './package.json'

export default {
  plugins: [
    vue(),
    WindiCSS()
  ],
  define: {
    APP_TITLE: JSON.stringify(pkg.name),
    APP_VERSION: JSON.stringify(buildVersion()),
    APP_REPOSITORY: JSON.stringify(pkg.repository),
    APP_LICENSE: JSON.stringify(pkg.license),
  },
  resolve: {
    alias: {
      "@": resolve(__dirname, "src"),
    },
  },
}

// FUNCTIONS
//----------

function buildVersion(): string {
  const version = pkg.version
  if (!isGitCommand()) {
    return version
  }
  const dirty = Number(execSync("git  status -s | wc -l").toString())
  return dirty > 0 ? `${version}…${dirty}` : version
}

function isGitCommand(): boolean {
  try {
    execSync("command -v git")
    return true
  } catch (e) {
    return false
  }
}
