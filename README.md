# VAPARA-VUE3

Vapara is a frontend application for online shopping.
Based on Vue3, it relies on Dolibarr's API calls.
`npm` version 7 required!

## HOW TO RUN

```
npm ci
npm run dev
```

## HOW TO DEPLOY

```bash
npm run linter && npm build
npm preview # optional
```

## MORE INFORMATION

- [TODO](TODO.md)
- [documentation](doc/INDEX.md)
- [license](./LICENSE)
